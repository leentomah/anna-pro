// Main.jsx
import React , {useState}from 'react';

import { Routes, Route, NavLink } from 'react-router-dom';
import Startseit from './Startseite'
import Kontakt from './Kontakt'
import Artikle from './Artikle'
import artikles from './artikles-content';
import Favoriten from './Favoriten'
import Impressum from './Impressum'




function Mainn({ addToFavorites, removeFromFavorites, favorites }) {




  return (
    <main>
      
        <Routes>
        <Route exact path="/"  element={<Startseit favorites={favorites} />}/>
        <Route path="/kontakt" element={<Kontakt />} />
        <Route path="/favoriten" element={<Favoriten favorites={favorites} removeFromFavorites={removeFromFavorites} />} />
        <Route path="/impressum" element={<Impressum />}  />
        <Route path="/artikle/:id" element={<Artikle addToFavorites={addToFavorites} favorites={favorites} removeFromFavorites={removeFromFavorites} />} />
       
        </Routes>
      
    </main>
  );
}
/*

        <Route path="/artikle" element={<ArtikleList />} />


const ArtikleList = () => {
  return (
    <div>
      {artikles.map((article) => (
        <Artikle
          key={article.id}
          id={article.id}
          title={article.title}
          content={article.content}
          author={article.author}
          image_url={article.image_url}
          date={article.date}
          time={article.time}
          sub_area={article.sub_area}
          summary={article.summary}
        />
      ))}
    </div>
  );
};
*/

/*
const Mainn = () => {

  //const { page } = this.props;

const page='';

  // Render different components based on the page prop
  if (page === 'Impressum') {
    return <Impressum />;
  } else if (page === 'Favoriten') {
    return <Favoriten />;
  }if (page === 'Kontakt') {
    return <Kontakt />;
  } else if (page === 'Artikle') {
  
    return  (artikles.map((article) => (
      <Artikle

        id={article.id}
        title={article.title}
        content={article.content}
        author={article.author}
        image_url={article.image_url}
        date={article.date}
        time={article.time}
        sub_area={article.sub_area}
        summary={article.summary}
      />
    )))
  }

  else {
    return <Startseite />; // Or render a default component or handle other cases
  }

return (
  <main>
  <h1>Willkommen zu Lifestyle Blog für Studierende und alle Interessierte</h1>

  <table className="overview-table">
    <thead>
      <tr>
        <th>Artikelüberschrift</th>
        <th>Autor</th>
        <th>Bildvorschau</th>
        <th>Veröffentlichungsdatum</th>
        <th>Favoriten</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="artikel1.html">Finanzen im Studium</a></td>
        <td>Anastassia Schmidt</td>
        <td><img src="img/Artikel1übersicht.jpeg" alt="Bild" height="52" width="52" /></td>
        <td>May 1, 2023</td>
        <td>215</td>
      </tr>
      <tr>
        <td><a href="artikel2.html">Trendy im Frühling</a></td>
        <td>Enas Zakrour</td>
        <td><img src="img/Artikel2übersicht.jpeg" alt="Bild" height="52" width="52" /></td>
        <td>May 3, 2023</td>
        <td>152</td>
      </tr>
      
    </tbody>
  </table>
</main>);

 
}*/

export default Mainn;
