import React, {useState ,useEffect} from 'react';
import Article_footer from './Article_footer';
import Rate from './Rate';
import artikles from './artikles-content';
import { useParams   } from 'react-router-dom';



function Artikle({ addToFavorites, removeFromFavorites, favorites })

    {
      

      const { id } = useParams();
      
      const art = artikles.find(item => item.id === id);

      const [isFavorite, setIsFavorite] = useState(false);
      const [charCount, setCharCount] = useState(0);
      const [inputValue, setInputValue] = useState('');

      console.log(favorites);


      useEffect(()=>{
      if( favorites.some((fav) => fav.id === id)){
        console.log("aaaaaa");

  setIsFavorite(true);
      }


},[])


const handleInputChange = (event) => {
  let value = event.target.value;

  if (value.length > 300) {
    value = value.slice(0, 300);
  }

  setInputValue(value);
  setCharCount(value.length);
};

      

      const handleFavoriteClick = () => {
        if (isFavorite) {
          removeFromFavorites(art);
        } else {
          addToFavorites(art);
        }
        setIsFavorite(!isFavorite);
      };
          

      
     
    
  return (

    <article>
      <figure>
        <img className="art-img" src={art.image_url} alt="Bild" />
        <figcaption>{art.title}</figcaption>
      </figure>

  
     

      <button id="favorite-button" className={`heart-button ${isFavorite ? 'filled' : ''}`}   onClick={handleFavoriteClick}  ></button>
 

      <p className='new_line'>{art.content} </p>
      <Rate clickHandler={(stars_num)=>{ console.log("ratting is "+stars_num)}} />


      <form  id="comment-form">
    <label >Bewertung:</label>
    <label htmlFor="comment">Kommentar:</label>
    <br/>
    <textarea id="comment" name="comment" rows="20" cols="80" maxLength="800" placeholder="schreib einen Kommentar" required value={inputValue} onChange={handleInputChange}></textarea>
    <br/>
    <span id="char-count">{charCount}/300</span>
    <br/>
    <input type="submit" value="Speichern"/>
  </form>
      <Article_footer
        sub_area={art.sub_area}
      summary={art.summary}
       author={art.author}
       profilePic={art.profilePic}
         date={art.date}
         time={art.time} />

<h4>
  Avareg rating for this article 
</h4>
<Rate star={3} />

      
    </article>
  );
}

export default Artikle;
