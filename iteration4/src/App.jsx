import React from 'react'
import MainView from './MainView'
//import './styles.css'
import { BrowserRouter as Router } from 'react-router-dom';


function App() {

  return (
    <>
   <Router>
    
     <MainView />
     
   </Router>

    </>
  )
}

export default App

