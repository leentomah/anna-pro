// Favoriten.jsx
import React from 'react';



const Favoriten = ({ favorites, removeFromFavorites }) => {



  const handleRemoveClick = (article) => {
    removeFromFavorites(article);
  };

  return (
    <div>
       <main>
      <h1 id="h1fav">Meine Lieblingsartikeln</h1>

      <table className="overview-table">
        <thead>
          <tr>
            <th>Artikelüberschrift</th>
            <th>Autor</th>
            <th>Bildvorschau</th>
            <th>Veröffentlichungsdatum</th>
            <th>Löschen</th>
            
          </tr>
        </thead>
        <tbody>


        {favorites.map((article) => (
          <tr key={article.id}>
          <td>  {article.title}  </td>
          <td>{article.author}</td>
          <td><img src={article.image_url} alt="Bild" height="52" width="52" /></td>
          <td>{article.date}</td>
          <td><button onClick={() => handleRemoveClick(article)}>Remove</button></td>
          
        </tr>
          
        ))}
         
        
        </tbody>
      </table>
      </main>
    </div>
  );
}

export default Favoriten;
