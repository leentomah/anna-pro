// Footer.jsx
import React from 'react';

const Footer = () => {
  return (

    <footer>
             <p>&copy; Anastassia Schmidt, Enas Zakrour – 2023</p>
    </footer>
  );
}

export default Footer;
