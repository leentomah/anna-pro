// Header.jsx
import React from 'react';
import { NavLink } from 'react-router-dom';


const Header = () => {
  return (
    <header>
      <nav>
      <NavLink  to="/">
          <img src="../img/logo.jpeg" alt="Logo" />
        </NavLink>
    <ul>
    <li><NavLink  to="/">Startseite</NavLink></li>
    <li><NavLink to="/favoriten">Favoriten</NavLink></li>
    <li><NavLink to="/kontakt">Kontakt</NavLink></li>
    <li><NavLink to="/impressum">Impressum</NavLink></li>
  
  </ul>
</nav>

    </header>
  );
}

export default Header;
