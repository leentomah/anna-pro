// Impressum.jsx
import React from 'react';



const Impressum = () => {
  return (
    <div>
      <main>
      <h1>impressum</h1>

      <h2>Authors</h2>

      <ul className="impressum-author">
        <li><strong>Name:</strong> Doe</li>
        <li><strong>Studiengang:</strong> Web Development</li>
        <li><strong>Hochschule:</strong> Example University</li>
      </ul>

      <ul className="impressum-author">
        <li><strong>Name:</strong> Smith</li>
        <li><strong>Studiengang:</strong> Graphic Design</li>
        <li><strong>Hochschule:</strong> Example University</li>
      </ul>
    </main>
    </div>
  );
}

export default Impressum;
