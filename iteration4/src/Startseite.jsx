// Main.jsx
import React from 'react';
import Artikle from './Artikle'
import artikles from './artikles-content';
import { Link } from 'react-router-dom';
import Rate from './Rate';

const Startseite = ({favorites}) => {

 





  return (

    <main>
      <h1>Willkommen zu Lifestyle Blog für Studierende und alle Interessierte</h1>

      <table className="overview-table">
        <thead>
          <tr>
            <th>Artikelüberschrift</th>
            <th>Autor</th>
            <th>Bildvorschau</th>
            <th>Veröffentlichungsdatum</th>
            <th>Favoriten</th>
            <th>avareg rate</th>

          </tr>
        </thead>
        <tbody>
        {artikles.map((article) => (



            <tr key={article.id}>
              <td>
                
                <Link to={`/artikle/${article.id}`} >{article.title}</Link>
              </td>
              <td>{article.author}</td>
              <td><img src={article.image_url} alt="Bild" height="52" width="52" /></td>
              <td>{article.date}</td>
              <td>{ favorites.some((fav) => fav.id === article.id)? '1' : '0' }</td>
              <td> <Rate star={2} /></td>
            </tr>
          ))}
          
        </tbody>
      </table>
    </main>
  );
}

export default Startseite;
