// Rate.jsx
import React from 'react';
import  { useState ,useEffect } from 'react';

const Rate = ({star,clickHandler}) => {



    const [rating, setRating] = useState(-1);


    useEffect(()=>{
      if( star == undefined){        

       setRating(-1);
      }else
    {
      setRating(star);
    }


},[])

 


  const handleStarClick = (s) => {
    
    if (rating == -1) {
      setRating(s);
      clickHandler(s);
      

      
    }
  };
    


  return (
    <div>
    {[1, 2, 3, 4, 5].map((s) => (
      <span
        key={s}
        onClick={() => handleStarClick(s)}
        style={{ color: rating && s <= rating ? 'gold' : 'gray', fontSize : "30px"} }
      >
        ★
      </span>
    ))}
  </div>
   
  );
}

export default Rate;
