// MainView.jsx
import React, {useState} from 'react';
import Header from './Header';
import Mainn from './Mainn';
import Footer from './Footer';



const MainView = () => {


  const [favorites, setFavorites] = useState([]);

  const addToFavorites = (article) => {
    setFavorites((prevFavorites) => [...prevFavorites, article]);
  };

  const removeFromFavorites = (article) => {
    setFavorites((prevFavorites) =>
      prevFavorites.filter((fav) => fav.id !== article.id)
    );
  };
  return (
    <div>
      <Header />
    
      <Mainn 
      addToFavorites={addToFavorites}
      removeFromFavorites={removeFromFavorites}
      favorites={favorites}
       />
      <Footer />
    </div>
  );
}

export default MainView;
