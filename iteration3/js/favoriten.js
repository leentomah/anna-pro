 


function check(id,article_id){

  const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
  const favoritesButton = document.getElementById(id);

  const isFavorite = favorites.some((fav) => fav.id === article_id);
  if (isFavorite) {

    favoritesButton.classList.add("filled");
  } else {

    favoritesButton.classList.remove("filled");
  }
}

// Nch der Start nachdem man den Button clickt
function handleFavoritesClick() {
  const favoritesButton = document.getElementById("favorite-button");

  const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
  //erste Treffer
  const isFavorite = favorites.some((fav) => fav.id === article.id);

      
  
    if (!isFavorite) {
        
        zuFavoritenHinzufuegen(article);   
          favoritesButton.classList.add("filled");
          localStorage.setItem("elementClasses", favoritesButton.classList.value);
          
       
       
    } else {
        zuFavoritenHinzufuegen(article, true);
         
          favoritesButton.classList.remove("filled");
          localStorage.setItem("elementClasses", favoritesButton.classList.value);

      
    }  
    }
  

  function zuFavoritenHinzufuegen(article, remove = false) {
    let favorites = localStorage.getItem('favorites');
    
    if (!favorites) {
      favorites = [];
    } else {
      favorites = JSON.parse(favorites);
    }
  
    if (remove) {
      // Remove article from favorites array
      favorites = favorites.filter((fav) => fav.id !== article.id);
      
    } else {
      // Add article to favorites array
      favorites.push(article);
      
    }
  
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }
  // irgend welche Name

  localStorage.setItem("favorites", JSON.stringify(favorites));


function displayFavorites() {
  const overviewTable = document.querySelector(".overview-table");
  const tableBody = overviewTable.querySelector("tbody");
  tableBody.innerHTML = ""; // Clear the table body before populating

  const favorites = JSON.parse(localStorage.getItem("favorites")) || [];

  favorites.forEach((article) => {
    // Create a new table row
    const row = document.createElement("tr");

    // Create table data for the article title
    const titleData = document.createElement("td");
    const titleLink = document.createElement("a");
    titleLink.href = article.url;
    titleLink.target = "_blank";
    titleLink.textContent = article.title;
    titleData.appendChild(titleLink);
    row.appendChild(titleData);

    // Create table data for the article author
    const authorData = document.createElement("td");
    authorData.textContent = article.author;
    row.appendChild(authorData);

    // Create table data for the article image
    const imageData = document.createElement("td");
    const image = document.createElement("img");
    image.src = article.image_url;
    image.alt = article.title;
    image.style.width = "50px";
    image.style.height = "50px"; // Adjust the size as needed
    imageData.appendChild(image);
    row.appendChild(imageData);

    // Create table data for the article publication date
    const dateData = document.createElement("td");
    dateData.textContent = article.date;
    row.appendChild(dateData);

    // Create table data for the delete button
    const deleteData = document.createElement("td");
    const deleteButton = document.createElement("button");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", () => {
      zuFavoritenHinzufuegen(article, true);
      displayFavorites(); // Update the display after deletion
    });
    deleteData.appendChild(deleteButton);
    row.appendChild(deleteData);

    // Append the row to the table body
    tableBody.appendChild(row);
  });
}
window.addEventListener("DOMContentLoaded", displayFavorites);
