document.addEventListener('DOMContentLoaded', function() {
    const commentInput = document.getElementById('comment');
    const charCount = document.getElementById('char-count');
  
    commentInput.addEventListener('input', function() {
      const numChar = commentInput.value.length;

      if (numChar > 300) {
        commentInput.value = commentInput.value.slice(0, 300);
        numChar = 300;
      }
      charCount.innerText = numChar + '/300';
    });
  });
  