import React from 'react';
import { Link } from 'react-router-dom';



const Favoriten = ({ favorites, removeFromFavorites }) => {
  
  const handleRemoveClick = (article) => {
    removeFromFavorites(article);
  };

  return (



    <main>
    <h1 id="h1fav">Meine Lieblingsartikeln</h1>

    <div className='row thead-row ' >
      <div class="col-12 col-md-3 ">Artikelüberschrift </div>
      <div class="col-12 col-md-2">Autor </div>
      <div class="col-12 col-md-2">Bildvorschau </div>
      <div class="col-12 col-md-3">Veröffentlichungsdatum </div>
      <div class="col-12 col-md-2">Löschen </div>
   
      </div>


    {favorites.map((article) => (
    <div className='row' key={article.id}>
             <div class="col-12 col-md-3">
             <Link to={`/artikle/${article.id}`}>{article.title}</Link>
              </div>
              <div class="col-12 col-md-2">
              {article.author}       
             </div>
             <div class="col-12 col-md-2">
             <img src={article.image_url} alt="Bild" height="52" width="52" />
              </div>
              <div class="col-12 col-md-3">
              {article.date}   
              </div>
              <div class="col-12 col-md-2">
              <button onClick={() => handleRemoveClick(article)} className='btn btn-danger'>Remove</button>
              </div>
              
              

              </div>
               ))}

   
      </main>
  );
}

export default Favoriten;
