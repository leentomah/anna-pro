// MainView.jsx
import React from 'react';



const Kontakt = () => {
  return (


    
    

  <main>
    
      <legend>Kontakt</legend>

    <form id="kontakt-form" className="form-horizontal">

  <fieldset>

    <div className="form-group">
      <label htmlFor="first-name" className='control-label col-sm-2 '>Vorname</label>
      <div class="col-sm-10">
      <input type="text" id="first-name" name="first-name" className="form-control" maxLength="40" minLength="2" pattern="[a-zA-Z]+ "  />
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="last-name" className='control-label col-sm-2'>Nachname</label>
      <div class="col-sm-10">
      <input type="text" id="last-name" name="last-name" className="form-control" maxLength="40" minLength="2" pattern="[a-zA-Z]+" />
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="email" className='control-label col-sm-2'>E-Mail*</label>
      <div class="col-sm-10">
      <input type="email" id="email" name="email" className="form-control" required />
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="article-number" className='control-label col-sm-2'>ArtiKelnummer*</label>
      <div class="col-sm-10">
      <input type="text" id="article-number" name="article-number" className="form-control" pattern="[a-z]{3}|[0-9]{8}|[0-9]{7}p" maxLength="8" required />
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="betreff" className='control-label col-sm-2'>Betreff*</label>
      <div class="col-sm-10">
      <input type="text" id="betreff" name="betreff" className="form-control" maxLength="40" minLength="2" pattern="[a-zA-Z]+" required />
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="category" className='control-label col-sm-2'>Kategorie</label>
      <div class="col-sm-10">
      <select id="category" name="category" className="form-control" required>
        <option value="">Select a category</option>
        <option value="Category 1">Mode</option>
        <option value="Category 2">Finanzen</option>
        <option value="Category 3">Essen</option>
      </select>
      </div>
    </div>
    <div className="form-group">
      <label htmlFor="description" className='control-label col-sm-2'>Beschreibung*</label>
      <div class="col-sm-10">
      <textarea id="description" name="description" rows="15" cols="40" className="form-control" maxLength="400" required></textarea>
    </div>
    </div>
    <em>*Pflichtfelder</em>
  </fieldset>
  <fieldset>
    <div className="button-group">
      <input type="reset" value="Formular löschen" className="btn btn-danger" style={{marginRight : "10px"} }/>
      <input type="submit" value="Senden" className="btn btn-warning" />
    </div>
  </fieldset>
</form>



  
  </main>


    
  );
}

export default Kontakt;