import React from 'react'
import MainView from './MainView'
import { BrowserRouter as Router } from 'react-router-dom';

import './styles.css'
 
function App() {

  return (
    <>
    <div className="container">
   <Router>
    
     <MainView />
     
   </Router>
   </div>
    </>
  )
}

export default App

