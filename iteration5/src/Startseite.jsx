import React, { useEffect ,useState} from 'react';
import Artikle from './Artikle';
// import artikles from './artikles-content';
import { Link } from 'react-router-dom';
import Rate from './Rate';

const Startseite = ({ favorites }) => {

  const [artikles, setartikles] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://fb1-ecommerce.hs-rw.de/60bb97fb-efe8-4dc1-9863-f35772583a58/articles'); // Replace with your API endpoint
        const jsonData = await response.json();
        setartikles(jsonData.data.articles);
        setLoading(false);
        console.log(artikles);
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchData();
  }, []);
  
  const formatDate = (dateString) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };
  if (loading) {
    return <p>Loading...</p>;
  }
  return (

<main>
      <h1>Willkommen zu Lifestyle Blog für Studierende und alle Interessierte</h1>
      
      <div className='row thead-row ' >
      <div className="col-12 col-md-2 ">Artikelüberschrift </div>
      <div className="col-12 col-md-2">Autor </div>
      <div className="col-12 col-md-2">Bildvorschau </div>
      <div className="col-12 col-md-2">Veröffentlichungsdatum </div>
      <div className="col-12 col-md-2">Favoriten </div>
      <div className="col-12 col-md-2">Durchschnittliche Bewertung </div>
   
      </div>
     

      {artikles.map((article) => (

<div className='row' key={article.id}>
             <div className="col-12 col-md-2">
             <Link to={`/artikle/${article.id}`}>{article.title}</Link>
              </div>
              <div className="col-12 col-md-2">
              {article.author}       
             </div>
             <div className="col-12 col-md-2">
             <img src={article.imageUrl} alt="Bild" height="52" width="52" />
              </div>
              <div className="col-12 col-md-2">
              {formatDate(article.publishedAt)}   
              </div>
              <div className="col-12 col-md-2">
              {favorites.some((fav) => fav.id === article.id) ? '1' : '0'}
              </div>
              <div className="col-12 col-md-2">
              <Rate star={2} />
              </div>

              

              </div>
            ))}

     
      
    </main>

    
/*

<main>
      <h1>Willkommen zu Lifestyle Blog für Studierende und alle Interessierte</h1>
      
        <table className="table">
          <thead>
            <tr>
              <th  scope="col">Artikelüberschrift</th>
              <th scope="col">Autor</th>
              <th  scope="col">Bildvorschau</th>
              <th scope="col">Veröffentlichungsdatum</th>
              <th  scope="col">Favoriten</th>
              <th  scope="col">Durchschnittliche Bewertung</th>
            </tr>
          </thead>
          <tbody>
            {artikles.map((article) => (
              <tr key={article.id} scope="row">
                <td >
                  <Link to={`/artikle/${article.id}`}>{article.title}</Link>
                </td>
                <td >{article.author}</td>
                <td >
                  <img src={article.image_url} alt="Bild" height="52" width="52" />
                </td>
                <td >{article.date}</td>
                <td >
                  {favorites.some((fav) => fav.id === article.id) ? '1' : '0'}
                </td>
                <td >
                  <Rate star={2} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      
    </main> */


 /*<main>
      <h1>Willkommen zu Lifestyle Blog für Studierende und alle Interessierte</h1>
      <div className="table-responsive">
        <table className="table">
          <thead>
            <tr>
              <th className="col-sm-12 col-md-2">Artikelüberschrift</th>
              <th className="col-sm-12 col-md-2">Autor</th>
              <th className="col-sm-12 col-md-2">Bildvorschau</th>
              <th className="col-sm-12 col-md-2">Veröffentlichungsdatum</th>
              <th className="col-sm-12 col-md-2">Favoriten</th>
              <th className="col-sm-12 col-md-2">Durchschnittliche Bewertung</th>
            </tr>
          </thead>
          <tbody>
            {artikles.map((article) => (
              <tr key={article.id}>
                <td className="col-12 col-md-2">
                  <Link to={`/artikle/${article.id}`}>{article.title}</Link>
                </td>
                <td className="col-12 col-md-2">{article.author}</td>
                <td className="col-12 col-md-2">
                  <img src={article.image_url} alt="Bild" height="52" width="52" />
                </td>
                <td className="col-12 col-md-2">{article.date}</td>
                <td className="col-12 col-md-2">
                  {favorites.some((fav) => fav.id === article.id) ? '1' : '0'}
                </td>
                <td className="col-12 col-md-2">
                  <Rate star={2} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </main>*/


   
  );
};

export default Startseite;
