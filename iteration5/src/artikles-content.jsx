import React from "react";


const artikles = [
    {
      id: 'artikle1',
      title: 'Finanzen im Studium',
      content:' Introduction: Mehr Freiheit oder mehr Einschränkungen?\n Abiturprüfungen abgelegt.\n Die Tinte getrocknet und die Noten stehen fest Die Anmeldung an einer Hochschule war erfolgreich und ab Oktober Adieu Hotel Mama,\n      hallo eigene Wohnung neue Freunde, neue Eindrücke, vielleicht sogar neue Liebe,  viel Freude und endlich Freiheit.\n   So denken viele Studierende in diesem neuen aufregenden Lebensabschnitt.\n   Aber wie soll man das finanzieren? Soll ich einen Job suchen? Wie passt das mit Studium \n  zusammen? \n    Müssen vielleicht sogar die Eltern mich weiter unterstützen? Wird das ausreichen? Oder soll ich einen Antrag bei Bafög stellen?\n \n Plan A Wie groß auch die Freude sein mag, um so genannte Freiheit ohne Eltern, muss man sich im Klaren sein, \ndass ab jetzt alle Haushaltssachen übernommen werden müssen, d.h nicht nur selbst kochen und aufräumen und und und, sondern auch den Möbeln, Kleinigkeiten und das Essen müssen gekauft werden. Dazu im weiteren Artikel mehr.  Du musst ein Plan haben und eine Struktur bekommen. Plan A- stell schnellstmöglich einen Antrag bei deinem BAföG Amt. Wie man das am besten tut, erfährst du in weiteren Artikeln. Oder du findest direkt Informationen vor Ort oder in deine Uni.  \nPlan B Wenn dein Antrag abgelehnt wurde , oder du bekommst nicht gesamte Summe, müssen somit deine Eltern dir etwas dazuzahlen, sobald dass für dich dein erstes Studium ist und du unter 30 Jahre alt bist .  \n Plan C Du gehst arbeiten. Hier entsteht die Frage- Ist das mit dem Studium zeitlich kompatibel. Vielleichst machst du eine Kombination von PlanA/B und Plan C.  Für was auch immer du dich entscheidest, vergiss nicht über deinen Grundwunsch, warum du das Elternhaus verlassen hast',
      author: 'Anastassia Schmidt',
      image_url: '../img/artikel1.jpeg',
      date: '01-05-2023',
      time: '03:00',
      sub_area: 'Finanzen',
      summary: ' Anastassia S wurde zu Hobbyautorin während sie einen Projekt in WebTec machte.\nSie verfasste mehrere Artikeln. Geboren wurde Frau Schmidt am 03.03.1995. Sie hat paar wichtige Rollen im Alltag: Sie arbeitet als Übersetzerin gelegentlich beim Bampf, studiert in HRW.  ',
      profilePic:'../img/ProfilAutor1.jpeg'
    },
    {
      
        id: 'artikle2',
        title: 'Trendy im Frühling',
        content:'Introduction: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla a semper neque. Sed eleifend urna id ante laoreet        bibendum. Mauris at est velit. Mauris sed nisl pharetra, fermentum    mauris sit amet, mattis sapien.   ',
        author: 'Enas Zakrour',
        image_url: '../img/artikel2.jpeg',
        date: '01-05-2023',
        time: '03:00',
        sub_area: 'Mode',
        summary: 'Ich bin Enas Zakrour und ich bin 26 Jahre alt . Meine Hobbys sind Bücher   lesen , Musik spielen. \n    Als ich 11 Jahre alt war ,habe ich mich dazu entschieden Autorin zu    werden.',
        profilePic:'../img/Profilautor_2.jpg'
      },
    // Add more articles here
  ];


  export default artikles;
 
    