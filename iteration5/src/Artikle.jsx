import React, { useState, useEffect } from 'react';
import Article_footer from './Article_footer';
import Rate from './Rate';
import artikles from './artikles-content';
import { useParams } from 'react-router-dom';

function Artikle({ addToFavorites, removeFromFavorites, favorites }) {
  const [art, setArt] = useState(null);
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();
  const [isFavorite, setIsFavorite] = useState(false);
  const [charCount, setCharCount] = useState(0);
  const [inputValue, setInputValue] = useState('');
  const [rating, setRating] = useState(0); // Rating state

  const formatDate = (dateString) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  const handlRatingClick = (stars_num) => {
    console.log('ratting is ' + stars_num);
    setRating(stars_num); // Update the rating state
  };

  const fetchArticle = async () => {
    try {
      var linknameArticle = `https://fb1-ecommerce.hs-rw.de/60bb97fb-efe8-4dc1-9863-f35772583a58/articles/${id}`;

      const response = await fetch(linknameArticle);
      const jsonData = await response.json();
      setArt(jsonData.data.article);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const fetchArticleComments = async () => {
    try {
      var linknameArticleComments = `https://fb1-ecommerce.hs-rw.de/60bb97fb-efe8-4dc1-9863-f35772583a58/articles/${id}/comments`;

      const response = await fetch(linknameArticleComments);
      const jsonData = await response.json();
      setComments(jsonData.data.comments);
      setLoading(false);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    fetchArticle();
    fetchArticleComments();

    if (favorites.some((fav) => fav.id === id)) {
      setIsFavorite(true);
    }
  }, []);

  const postComment = async (comment) => {
    console.log('postcccc');
    console.log(JSON.stringify(comment));
    try {
      var linknameArticleComments = `https://fb1-ecommerce.hs-rw.de/60bb97fb-efe8-4dc1-9863-f35772583a58/articles/${id}/comments`;
      const response = await fetch(linknameArticleComments, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(comment),
      });

      if (response.ok) {
        console.log('Comment posted successfully');
      } else {
        console.error('Failed to post comment');
      }
    } catch (error) {
      console.error('Error:', error);
    }
    fetchArticleComments();
  };

  const handleSubmit = (event) => {
    console.log(rating);
    event.preventDefault();
    // Handle the form submission here
    const comment = {
      content: inputValue,
      author: event.target.elements.author.value,
      title: event.target.elements.title.value,
      rating: rating, // Use the rating state value
    };
    console.log(comment);
    postComment(comment);
    // Reset form fields
    event.target.reset();
    setInputValue('');
    setCharCount(0);
    setRating(0); // Reset the rating state
  };

  const handleInputChange = (event) => {
    let value = event.target.value;

    if (value.length > 300) {
      value = value.slice(0, 300);
    }

    setInputValue(value);
    setCharCount(value.length);
  };

  const handleFavoriteClick = () => {
    if (isFavorite) {
      removeFromFavorites(art);
    } else {
      addToFavorites(art);
    }
    setIsFavorite(!isFavorite);
  };

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    <article>
      <figure>
        <img className="art-img" src={art.imageUrl} alt="Bild" />
        <figcaption>{art.title}</figcaption>
      </figure>

      <button
        id="favorite-button"
        className={`heart-button ${isFavorite ? 'filled' : ''}`}
        onClick={handleFavoriteClick}
      ></button>

      <p className="new_line">{art.content} </p>
      <div></div>
      <h6>Bewertung</h6>
      <Rate clickHandler={handlRatingClick} />

      <form id="comment-form" className="form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="comment">Kommentar:</label>
          <textarea
            id="comment"
            name="comment"
            rows="10"
            cols="80"
            className="form-control"
            maxLength="800"
            placeholder="Schreibe einen Kommentar"
            required
            value={inputValue}
            onChange={handleInputChange}
          />
        </div>

        <span id="char-count">{charCount}/300</span>
        <div className="form-group">
          <label htmlFor="author">Name:</label>
          <input id="author" name="author" className="form-control" placeholder="Enter your name" required />
        </div>
        <div className="form-group">
          <label htmlFor="title">Title:</label>
          <input id="title" name="title" className="form-control" placeholder="title of the comments" required />
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-style ">
            Speichern
          </button>
        </div>
      </form>

      {comments.map((comment) => (
        <div key={comment.id}>
          <h4>{comment.title}</h4>
          <p>Author: {comment.author}</p>
          <p>{comment.content}</p>
          <p>Published At: {formatDate(comment.publishedAt)}</p>
          <p>Rating: {comment.rating}</p>
          <Rate star={comment.rating} />
          <hr />
        </div>
      ))}

      <Article_footer
        sub_area={art.sub_area}
        summary={art.summary}
        author={art.author}
        profilePic={art.profilePic}
        date={art.date}
        time={art.time}
      />

      <h4>Avareg rating for this article</h4>
      <Rate star={3} />
    </article>
  );
}

export default Artikle;
