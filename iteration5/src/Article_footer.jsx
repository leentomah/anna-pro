// Article_footer.jsx
import React from 'react';

const Article_footer = ({ sub_area,summary, author, profilePic, date,time }) => {
  return (
   <div> 
       <figure>
    
    <img className="art-img" src={profilePic} alt="Bild" height="62" width="52"/>
    <figcaption>Authorin: {author} </figcaption>
  </figure>
    <p className="summaryKurzvita">{summary}    </p>
                
        
        
      
    <p> veröffentlicht:<time>{date}</time></p>
      <p>Durschnittliche Lesezeit: <time>{time}</time>minutes</p>
      <p className="subject-area">{sub_area}</p>
      <p className="favoriten-count">favoriten: 215</p>
     
      </div>
   
  );
}

export default Article_footer;
