// Impressum.jsx
import React from 'react';
import Card from 'react-bootstrap/Card';



const Impressum = () => {

  const handleEmailClick = (email) => {
    window.location.href = `mailto:${email}`;
  };
  return (
    <div>
    <main>
      <h1>Impressum</h1>

      <h2>Authors</h2>

      <div>
        <Card
          bg="light"
          style={{ width: '18rem', marginBottom: '10px', boxShadow: '10px 10px 15px silver' }}

        >
          <Card.Header onClick={() => handleEmailClick('john@example.com')}>
            Doe <a href={`mailto:john@example.com`}>✉</a>
          </Card.Header>
          <Card.Body>
            <Card.Text>
              Studiengang: Web Development
              <br />
              Hochschule: Example University
            </Card.Text>
          </Card.Body>
        </Card>
      </div>

      <div >
        <Card
          bg="light"
          style={{ width: '18rem', marginBottom: '10px', boxShadow: '10px 10px 15px silver' }}

        >
          <Card.Header onClick={() => handleEmailClick('jane@example.com')}>
            Smith <a href={`mailto:jane@example.com`}>✉</a>
          </Card.Header>
          <Card.Body>
            <Card.Text>
              Studiengang: Graphic Design
              <br />
              Hochschule: Example University
            </Card.Text>
          </Card.Body>
        </Card>
      </div>
    </main>
  </div>
  );
}

export default Impressum;
